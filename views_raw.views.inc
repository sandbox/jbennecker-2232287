<?php

function views_raw_views_plugins() {
	$plugins = array();

	$plugins['style']['raw'] = array(
		'title' => t('Raw'),
		'handler' => 'views_raw_plugin_style',
		'theme' => 'views_raw_view',
		'uses row plugin' => FALSE,
		'uses fields' => TRUE,
		'uses options' => FALSE,
		'type' => 'normal'
		);

	return $plugins;
}