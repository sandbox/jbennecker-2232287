This Module is a Views Style Plugin.
It adds a Views Style which will not output anything, but provides the views result data in $rows.
It is aimed at Developers.

Install

1. Extract into sites/all/modules/views_raw
2. Enable the Module

Usage

Templates
1. Create a View and Select Raw as its Style.
2. Create a new Template File for your View. For Example: /sites/all/themes/[[theme_name]]/templates/views-view--[[view_name]].tpl.php. In $rows you'll have the raw data of views result.

Make your own Views Style Plugin and use this Module as Sceleton
1. Copy/adjust as you like